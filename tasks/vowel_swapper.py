def vowel_swapper(string):
    # ==============
    # Your code here

    for x in string:
        if x == "O":
            string = string.replace("O", '000')
            
    string = string.lower()
    
    for x in string:
        if x == "a" or "A":
            string = string.replace("a",'4')
        if x == "e" or "E":
            string = string.replace("e",'3')
        if x == "i" or "I":
            string = string.replace("i",'!')
        if x == "o":
            string = string.replace("o",'ooo')
        if x == "u" or "U":
            string = string.replace("u",'|_|')

    string = string.capitalize()
    
    return string

    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console