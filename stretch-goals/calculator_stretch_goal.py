def calculator(a, b, operator):
    # ==============
    # Your code here

    a = int(a)
    b = int(b)

    if operator == '+':
        answer = a + b
    elif operator == '-':
        answer = a - b
    elif operator == '*':
        answer = a * b
    elif operator == '/':
        answer = a / b

    answer = int(answer)

    return "{0:b}".format(answer)

    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
