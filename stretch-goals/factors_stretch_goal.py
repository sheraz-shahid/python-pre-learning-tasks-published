def factors(number):
    # ==============
    # Your code here

    factor_list = []
    
    for i in range (1,number+1):
        if number % i == 0:
            factor_list.append(i)


    factor_list.pop(0)
    factor_list.pop(-1)


    if not factor_list:
        return (number, "is a prime number")
    else:
        return factor_list


    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
